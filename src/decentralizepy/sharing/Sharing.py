import json
import logging
from collections import deque
import re
from decentralizepy import datasets

from decentralizepy.datasets.Dataset import Dataset
import hashlib

import numpy as np
import torch
import resource

class Sharing:
    """
    API defining who to share with and what, and what to do on receiving

    """
    

    def __init__(
        self, rank, machine_id, communication, mapping, graph, model, dataset, log_dir
    ):
        """
        Constructor

        Parameters
        ----------
        rank : int
            Local rank
        machine_id : int
            Global machine id
        communication : decentralizepy.communication.Communication
            Communication module used to send and receive messages
        mapping : decentralizepy.mappings.Mapping
            Mapping (rank, machine_id) -> uid
        graph : decentralizepy.graphs.Graph
            Graph reprensenting neighbors
        model : decentralizepy.models.Model
            Model to train
        dataset : decentralizepy.datasets.Dataset
            Dataset for sharing data. Not implemented yer! TODO
        log_dir : str
            Location to write shared_params (only writing for 2 procs per machine)

        """
        self.rank = rank
        self.machine_id = machine_id
        self.uid = mapping.get_uid(rank, machine_id)
        self.communication = communication
        self.mapping = mapping
        self.graph = graph
        self.model = model
        self.dataset = dataset
        self.communication_round = 0
        self.log_dir = log_dir
        self.total_model = 0
        NB_SAMPLES_TO_SHARE = 0
        list_of_indices = []
        number_of_samples = 0
        memory_used = 0


        self.peer_deques = dict()
        my_neighbors = self.graph.neighbors(self.uid)
        for n in my_neighbors:
            self.peer_deques[n] = deque()

    def received_from_all(self):
        """
        Check if all neighbors have sent the current iteration

        Returns
        -------
        bool
            True if required data has been received, False otherwise

        """
        for _, i in self.peer_deques.items():
            if len(i) == 0:
                return False
        return True

    def get_neighbors(self, neighbors):
        """
        Choose which neighbors to share with

        Parameters
        ----------
        neighbors : list(int)
            List of all neighbors

        Returns
        -------
        list(int)
            Neighbors to share with

        """
        # modify neighbors here
        return neighbors

    def serialized_model(self):
        """
        Convert model to json dict. Here we can choose how much to share

        Returns
        -------
        dict
            Model converted to json dict

        """
        m = dict()
        
        
        for key, val in self.model.state_dict().items():
            m[key] = json.dumps(val.numpy().tolist())
            self.total_model += len(self.communication.encrypt(m[key]))
        
        return m
    
    
    def deserialized_model(self, m):
        """
        Convert received json dict to state_dict.

        Parameters
        ----------
        m : dict
            json dict received

        Returns
        -------
        state_dict
            state_dict of received

        """
        state_dict = dict()
        for key, value in m.items():
            state_dict[key] = torch.from_numpy(np.array(json.loads(value)))
        return state_dict

    def share_data_indices(self):
        
        """
        Share data using indices method.

        Returns
        -------
        dict
            samples that will be send

        """
    
        m = dict()
        model_sharing_nb_bytes_per_neighbor = 500000
        global NB_SAMPLES_TO_SHARE
        
        if self.communication_round == 0:
            nb_samples = model_sharing_nb_bytes_per_neighbor/ (self.dataset.train_x[0].nbytes + self.dataset.train_y[0].nbytes)
            NB_SAMPLES_TO_SHARE = nb_samples
        

        rate = np.random.randint(0,self.dataset.train_x.shape[0],size=round(NB_SAMPLES_TO_SHARE))
        
        rate = rate.tolist()
        
        global list_of_indices
        if self.communication_round == 0:
          list_of_indices= []
        
        for x in rate:
            if x in list_of_indices:
                rate.remove(x)
        
        list_of_indices = list_of_indices + rate

        m['x'] = json.dumps(np.take(self.dataset.train_x, rate, axis=0).tolist())
        m['y'] = json.dumps(np.take(self.dataset.train_y, rate, axis=0).tolist())
        
        return m

    
    
    def share_data(self):

        """
        Share data.

        Returns
        -------
        dict
            samples that will be send

        """

        m = dict()
        
        model_sharing_nb_bytes_per_neighbor = 500000
        global NB_SAMPLES_TO_SHARE
        
        if self.communication_round == 0:
            nb_samples = model_sharing_nb_bytes_per_neighbor/ (self.dataset.train_x[0].nbytes + self.dataset.train_y[0].nbytes)
            NB_SAMPLES_TO_SHARE = nb_samples
        
        rate = np.random.randint(0,self.dataset.train_x.shape[0],size=round(NB_SAMPLES_TO_SHARE))


        m['x'] = json.dumps(np.take(self.dataset.train_x, rate, axis=0).tolist())
        m['y'] = json.dumps(np.take(self.dataset.train_y, rate, axis=0).tolist())
    
        return m


    
    def hash_array(arr):

        """
        Hash of a 4D array .

        Parameters
        ----------
        m : array

        Returns
        -------
        
        hash of the array

        """
        
        if not arr.flags['C_CONTIGUOUS']:
            arr = np.ascontiguousarray(arr)
        
        return hashlib.sha256(arr).hexdigest()



    def remove_duplicates(self,a, b, c, hashfunc=hash_array):

        """
        Remove duplicates.

        Parameters
        ----------
        a : Original dataset (without new samples) without labels
        b : Array with new samples
        c : Lables of the original Dataset

        Returns
        -------
        
        array with new samples (X) and (y) after cheking duplicates

        """
        
        table_a = dict()
        table_b = dict()

        for i, a_i in enumerate(a):
            if hashfunc(a_i) in table_a:

                table_a[hashfunc(a_i)].append(i)
            else:
                table_a[hashfunc(a_i)] = [i]
    
        for j, b_j in enumerate(b):
            if hashfunc(b_j) in table_b:
                table_b[hashfunc(b_j)].append(j)
            else:
                table_b[hashfunc(b_j)] = [j]
    
        intersection = table_a.keys() & table_b.keys()
    
        indices = []
        for k in intersection:
        
            index_to_delete = table_b.get(k)
            for elem in index_to_delete:
                indices.append(elem)
        
        
        arr1 = np.delete(b,indices,axis=0)
        arr2 = np.delete(c,indices,axis=0)

    
        return arr1,arr2

    def total_comparaison(self,x,y):

        """
        Remove duplicates using total comparaison_method.

        Parameters
        ----------
        x : Array with new samples (X)
        y : Labels of new smaples (y)

        Returns
        -------
        
        Updated dataset (X) and (y)
        
        """

        new_dataset_x = np.concatenate((self.dataset.train_x, x),axis=0)
        self.dataset.train_x, indices = np.unique(new_dataset_x,axis=0,return_index=True)
        new_dataset_y = np.concatenate((self.dataset.train_y, y),axis=0)
        self.dataset.train_y = np.take(new_dataset_y, indices)

        return self.dataset.train_x, self.dataset.train_y

    def remove_after_hash(self, x, y):

        """
        Remove duplicates using hash method.

        Parameters
        ----------
        x : Array with new samples (X)
        y : Labels of new smaples (y)

        Returns
        -------
        
        Updated dataset (X) and (y)
        
        """
        new_x, new_y = self.remove_duplicates(self.dataset.train_x,x,y)

        self.dataset.train_x = np.concatenate((self.dataset.train_x, new_x), axis=0)
        self.dataset.train_y = np.concatenate((self.dataset.train_y, new_y), axis=0)

        return self.dataset.train_x, self.dataset.train_y



        
    
    def update_data(self,recieved_data):

        """
        Update dataset

        Parameters
        ----------
       
        recieved_data: Recieved data in a dict

        Returns
        -------
        
        Recieved data
        
        """

        data = dict()
        for key, value in recieved_data.items():
            if key == 'x':
                data[key] = torch.from_numpy(np.array(json.loads(value))).float().numpy()
            if key == 'y':
                data[key] = torch.from_numpy(np.array(json.loads(value))).long().numpy()
        
        #Update data after checking duplicates using total comparaison method
        self.dataset.train_x, self.dataset.train_y = self.total_comparaison(self, data['x'], data['y'])
        
        #Update data after checking duplicates using indices method
        self.dataset.train_x = np.concatenate((self.dataset.train_x, data['x']),axis=0)
        self.dataset.train_y = np.concatenate((self.dataset.train_y, data['y']),axis=0)

        #Update data after checking duplicates using hash method
        self.dataset.train_x, self.dataset.train_y = self.remove_after_hash(self, data['x'], data['y'])

        del recieved_data['x']
        del recieved_data['y']

        return recieved_data


    #Step function for Data sharing. Please comment the step function below!

    def step(self):
        """
        Perform a sharing step. Implements D-PSGD.

        """
        model = self.serialized_model()
        my_uid = self.mapping.get_uid(self.rank, self.machine_id)
        all_neighbors = self.graph.neighbors(my_uid)
        iter_neighbors = self.get_neighbors(all_neighbors)
        model["degree"] = len(all_neighbors)
        model["iteration"] = self.communication_round
        for neighbor in iter_neighbors:
            self.communication.send(neighbor,model)
            

        logging.info("Waiting for messages from neighbors")
        while not self.received_from_all():
            sender, model = self.communication.receive()
            logging.info("Received model from {}".format(sender))
            degree = model["degree"]
            iteration = model["iteration"]
            del model["degree"]
            del model["iteration"]
            
            self.peer_deques[sender].append((degree, iteration, model))

            logging.info(
                "Deserialized received model from {} of iteration {}".format(
                    sender, iteration
                )
            )
        


        logging.info("Starting model averaging after receiving from all neighbors")
        total = dict()
        weight_total = 0
        for i, n in enumerate(self.peer_deques):
            degree, iteration, data = self.peer_deques[n].popleft()
            logging.debug(
                "Averaging model from neighbor {} of iteration {}".format(n, iteration)
            )
            data = self.deserialized_model(data)
            weight = 1 / (max(len(self.peer_deques), degree) + 1)  # Metro-Hastings
            weight_total += weight
            for key, value in data.items():
                if key in total:
                    total[key] += value * weight
                else:
                    total[key] = value * weight

        for key, value in self.model.state_dict().items():
            total[key] += (1 - weight_total) * value  # Metro-Hastings

        self.model.load_state_dict(total)

        logging.info("Model averaging complete")

        self.communication_round += 1


    #Step function for Data sharing. Please comment the step function above!
    
    def step(self):
        
        data = self.shareData()
        my_uid = self.mapping.get_uid(self.rank, self.machine_id)
        all_neighbors = self.graph.neighbors(my_uid)
        iter_neighbors = self.get_neighbors(all_neighbors)
        data["degree"] = len(all_neighbors)
        data["iteration"] = self.communication_round
        for neighbor in iter_neighbors:
            self.communication.send(neighbor,data)

        logging.info("Waiting for messages from neighbors")
        while not self.received_from_all():
            sender, data = self.communication.receive()
            logging.info("Received data from {}".format(sender))
            degree = data["degree"]
            iteration = data["iteration"]
            del data["degree"]
            del data["iteration"]
            
            self.peer_deques[sender].append((degree, iteration, data))

           
        logging.info("Starting updating dataset after receiving from all neighbors")
        for i, n in enumerate(self.peer_deques):
            degree, iteration, data = self.peer_deques[n].popleft()
            data_updated = self.update_data(data)

        logging.info("Updating database complete")

        logging.info("Shape of the dataset is {}".format(self.dataset.train_x.shape))

        self.communication_round += 1

