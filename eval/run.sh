#!/bin/bash

decpy_path=/home/abdennad/Gitlab/decentralizepy/eval
cd $decpy_path

env_python=/home/abdennad/miniconda3/envs/decpy/bin/python3
graph=25_nodes_ring_graph.edges
original_config=step_configs/config_femnist.ini
config_file=/home/abdennad/config.ini
procs_per_machine=5
machines=5
iterations=45
test_after=1
eval_file=testing.py
log_level=INFO

m=`cat $(grep addresses_filepath $original_config | awk '{print $3}') | grep $(/sbin/ifconfig ens785 | grep 'inet ' | awk '{print $2}') | cut -d'"' -f2`

##cp $original_config $config_file
##echo "alpha = 0.75" >> $config_file
#$env_python $eval_file -mid $m -ps $procs_per_machine -ms $machines -is $iterations -gf $graph -ta $test_after -cf $config_file -ll $log_level

#cp $original_config $config_file
#echo "alpha = 0.50" >> $config_file
#$env_python $eval_file -mid $m -ps $procs_per_machine -ms $machines -is $iterations -gf $graph -ta $test_after -cf $config_file -ll $log_level

#cp $original_config $config_file
#echo "alpha = 0.10" >> $config_file
#$env_python $eval_file -mid $m -ps $procs_per_machine -ms $machines -is $iterations -gf $graph -ta $test_after -cf $config_file -ll $log_level

$env_python $eval_file -mid $m -ps $procs_per_machine -ms $machines -is $iterations -gf $graph -ta $test_after -cf $original_config -ll $log_level


